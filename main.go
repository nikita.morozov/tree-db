package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"google.golang.org/grpc/health"

	"gitlab.com/aquix/ms-generator/tree/delivery/http"
	"gitlab.com/aquix/ms-generator/tree/delivery/proto"
	"gitlab.com/aquix/ms-generator/tree/models"
	"gitlab.com/aquix/ms-generator/tree/repositories/gormHandler"
	"gitlab.com/aquix/ms-generator/tree/usecases"

	"gitlab.com/nikita.morozov/ms-shared/middleware"
	"gitlab.com/nikita.morozov/ms-shared/tools"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"log"
	"net"
	"os"
)

func main() {
	// Load env variables
	serverAddress := os.Getenv("HTTP_SERVER")
	grpcServerAddress := os.Getenv("GRPC_SERVER")
	dbHost := os.Getenv("DB_HOST")
	dbUser := os.Getenv("DB_USERNAME")
	dbName := os.Getenv("DB_DATABASE")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbPort := os.Getenv("DB_PORT")

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s", dbHost, dbUser, dbPassword, dbName, dbPort)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "tree_ms-",
		},
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	err = models.NewGormMigration(db).Migrate()
	if err != nil {
		log.Fatal(err)
		return
	}

	// Init grpc server
	lis, err := net.Listen("tcp", grpcServerAddress)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)

	e := echo.New()
	e.HideBanner = true
	middL := middleware.InitMiddleware()
	e.Use(middL.CORS)

	// Init repository level

	entityRepo := gormHandler.NewEntityRepository(db, 10)

	// Init use cases layer
	entityUseCase := usecases.NewEntityUseCase(entityRepo)

	// Init delivery layer
	http.NewEntityHttpHandler(e, entityUseCase)

	// Init GRPC delivery layer
	proto.NewEntityGrpcHandler(grpcServer, entityUseCase)
	grpc_health_v1.RegisterHealthServer(grpcServer, health.NewServer())

	tools.DisplayServiceName("tree-ms")

	//Start GRPC server
	log.Printf("Start GRPC server on address: %s", grpcServerAddress)
	go func() {
		err = grpcServer.Serve(lis)
		if err != nil {
			log.Fatal(err)
		}
	}()

	// Start HTTP server
	err = e.Start(serverAddress)
	if err != nil {
		log.Fatal(err)
	}
}
