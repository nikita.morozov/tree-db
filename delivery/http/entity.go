package http

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/aquix/ms-generator/tree/models"
	"gitlab.com/aquix/ms-generator/tree/models/dto"
	"gitlab.com/aquix/ms-generator/tree/usecases"
	"gitlab.com/nikita.morozov/ms-shared/models/response"
	"net/http"
	"strconv"
)

type EntityHttpHandler struct {
	entityUC usecases.EntityUseCases
}

func (h *EntityHttpHandler) Create(c echo.Context) error {
	var model models.Entity
	err := c.Bind(&model)
	if err != nil {
		return err
	}
	var item = &models.Entity{
		Type:      model.Type,
		Data:      model.Data,
		Slug:      model.Slug,
		ChildType: model.ChildType,
		Title:     model.Title,
		ParentId:  model.ParentId,
	}
	err = h.entityUC.Create(item)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusCreated, response.ItemResp{Item: item})
}

func (h *EntityHttpHandler) Update(c echo.Context) error {
	var model models.Entity
	err := c.Bind(&model)
	if err != nil {
		return err
	}

	res, err := h.entityUC.Update(&model)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, response.ItemResp{Item: res})
}

func (h *EntityHttpHandler) Delete(c echo.Context) error {
	var model dto.IdReq
	err := c.Bind(&model)
	if err != nil {
		return err
	}

	err = h.entityUC.Delete(model.Id)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *EntityHttpHandler) GetById(c echo.Context) error {
	idStr := c.Param("id")
	id, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		return err
	}

	item, err := h.entityUC.GetById(id)
	if err != nil {
		return err
	}

	count, err := h.entityUC.CountById(&item.ID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, dto.GetBySlugResp{Item: item, ChildrenCount: uint64(count)})
}

func (h *EntityHttpHandler) Count(c echo.Context) error {
	count, err := h.entityUC.Count()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, dto.CountResp{Count: count})
}

func (h *EntityHttpHandler) GetByType(c echo.Context) error {
	var model dto.TypeDto
	err := c.Bind(&model)
	if err != nil {
		return err
	}

	item, err := h.entityUC.GetByType(model.Type)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, response.ItemResp{Item: item})
}

func (h *EntityHttpHandler) GetBySlug(c echo.Context) error {
	slug := c.Param("slug")
	item, err := h.entityUC.GetBySlug(slug)
	if err != nil {
		return err
	}

	count, err := h.entityUC.CountById(&item.ID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, dto.GetBySlugResp{Item: item, ChildrenCount: uint64(count)})
}

func (h *EntityHttpHandler) ListByParentId(c echo.Context) error {
	var model dto.ListByParentIdDto
	err := c.Bind(&model)
	if err != nil {
		return err
	}

	list, paginator, err := h.entityUC.ListByParentId(model.ParentId, model.Opt)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, dto.ListWithParentResp{List: list, Paginator: paginator})
}

func (h *EntityHttpHandler) ListBySlug(c echo.Context) error {
	var model dto.ListBySlugDto
	err := c.Bind(&model)
	if err != nil {
		return err
	}

	var parent *models.Entity
	var pip *uint64

	if model.Slug != nil {
		parent, err = h.entityUC.GetBySlug(*model.Slug)
		if err != nil {
			return err
		}

		pip = &parent.ID
	}

	list, paginator, err := h.entityUC.ListByParentId(pip, model.Opt)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, dto.ListWithParentResp{List: list, Paginator: paginator})
}

func NewEntityHttpHandler(e *echo.Echo, entityUC usecases.EntityUseCases) {
	handler := EntityHttpHandler{
		entityUC: entityUC,
	}

	e.POST("/api/v1/entity/create", handler.Create)
	e.PUT("/api/v1/entity/update", handler.Update)
	e.DELETE("/api/v1/entity/delete", handler.Delete)
	e.GET("/api/v1/entity/get-by-id/:id", handler.GetById)
	e.GET("/api/v1/entity/count", handler.Count)
	e.POST("/api/v1/entity/get-by-type", handler.GetByType)
	e.GET("/api/v1/entity/by-slug/:slug", handler.GetBySlug)
	e.POST("/api/v1/entity/list-by-parent-id", handler.ListByParentId)
	e.POST("/api/v1/entity/list-by-slug", handler.ListBySlug)
}
