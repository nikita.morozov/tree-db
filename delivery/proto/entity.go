package proto

import (
	"context"
	"gitlab.com/aquix/ms-generator/tree/models"
	pb "gitlab.com/aquix/ms-generator/tree/proto"
	"gitlab.com/aquix/ms-generator/tree/usecases"
	models2 "gitlab.com/nikita.morozov/ms-shared/models"
	shared "gitlab.com/nikita.morozov/ms-shared/models"
	"gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	"time"
)

type entityServer struct {
	pb.UnimplementedEntityServer
	entityUC usecases.EntityUseCases
}

func CreateModelFromProto(in *pb.EntityModel) models.Entity {
	var data models2.JSONB
	data.Scan(in.Data)

	var path models2.JSONBUint64
	path.Scan(in.Path)

	parentId := in.ParentId

	return models.Entity{
		BaseUInt64Model: shared.BaseUInt64Model{
			ID: in.Id,
		},
		Type:     in.Type,
		Data:     data,
		Title:    in.Title,
		ParentId: parentId,
		Path:     path,
	}
}

func CreateProtoFromModel(item models.Entity) *pb.EntityModel {
	data, _ := item.Data.Value()
	path, _ := item.Path.Value()

	var DeletedValue *int64
	deletedAt, _ := item.DeletedAt.Value()
	if deletedAt != nil {
		value := deletedAt.(time.Time).Unix()
		DeletedValue = &value
	}

	return &pb.EntityModel{
		Id:        item.ID,
		CreatedAt: item.CreatedAt.Unix(),
		UpdatedAt: item.UpdatedAt.Unix(),
		DeletedAt: DeletedValue,
		Type:      item.Type,
		Data:      string(data.(string)),
		Title:     item.Title,
		ParentId:  item.ParentId,
		Path:      string(path.([]uint8)),
	}
}

func (s *entityServer) Create(ctx context.Context, in *pb.EntityModel) (*pb.EntityModel, error) {
	item := CreateModelFromProto(in)
	item.Path = nil

	err := s.entityUC.Create(&item)
	if err != nil {
		return nil, err
	}

	return CreateProtoFromModel(item), nil
}

func (s *entityServer) Update(ctx context.Context, in *pb.EntityModel) (*pb.EntityModel, error) {
	item := CreateModelFromProto(in)

	itemRes, err := s.entityUC.Update(&item)
	if err != nil {
		return nil, err
	}

	return CreateProtoFromModel(*itemRes), nil
}

func (s *entityServer) Delete(ctx context.Context, in *proto.Id64Request) (*emptypb.Empty, error) {
	err := s.entityUC.Delete(in.Id)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (s *entityServer) GetById(ctx context.Context, in *proto.Id64Request) (*pb.EntityModel, error) {
	item, err := s.entityUC.GetById(in.Id)
	if err != nil {
		return nil, err
	}

	return CreateProtoFromModel(*item), nil
}

func (s *entityServer) Count(ctx context.Context, _ *emptypb.Empty) (*pb.CountEntityResponse, error) {
	count, err := s.entityUC.Count()
	if err != nil {
		return nil, err
	}

	resp := pb.CountEntityResponse{
		Count: count,
	}

	return &resp, nil
}

func (s *entityServer) GetByType(ctx context.Context, in *proto.StringRequest) (*pb.EntityModel, error) {
	item, err := s.entityUC.GetByType(in.Value)
	if err != nil {
		return nil, err
	}
	return CreateProtoFromModel(*item), nil
}

func (s *entityServer) ListByParentId(ctx context.Context, in *pb.ListByParentIdReq) (*pb.ListByParentIdResp, error) {
	modelsOpts := models2.ListOptions{
		Order:  in.Opts.Order,
		Limit:  int(in.Opts.Limit),
		Offset: int(in.Opts.Offset),
	}

	var parentId *uint64
	if in.ParentId > uint64(0) {
		parentId = &in.ParentId
	}

	list, _, err := s.entityUC.ListByParentId(parentId, modelsOpts)
	if err != nil {
		return nil, err
	}

	resp := pb.ListByParentIdResp{
		Paginator: &proto.Paginator{
			Offset: int64(in.Opts.Offset),
			//TODO: Need implement
			TotalCount:   0,
			CountPerPage: 10,
		},
		Items: []*pb.EntityModel{},
	}

	if list != nil {
		for _, item := range *list {
			res := CreateProtoFromModel(item)
			resp.Items = append(resp.Items, res)
		}
	}

	return &resp, nil
}

func NewEntityGrpcHandler(s *grpc.Server, entityUC usecases.EntityUseCases) {
	pb.RegisterEntityServer(s, &entityServer{entityUC: entityUC})
}
