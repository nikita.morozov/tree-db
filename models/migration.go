package models

import "gorm.io/gorm"

type Migration interface {
	Migrate() error
}

type gormMigration struct {
	DB *gorm.DB
}

func (g gormMigration) Migrate() error {
	var err error
	err = g.DB.Migrator().AutoMigrate(&Entity{})
	if err != nil {
		return err
	}
	return err
}

func NewGormMigration(DB *gorm.DB) Migration {
	return &gormMigration{
		DB: DB,
	}
}
