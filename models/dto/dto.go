package dto

import (
	"gitlab.com/aquix/ms-generator/tree/models"
	models2 "gitlab.com/nikita.morozov/ms-shared/models"
)

type IdReq struct {
	Id uint64 `json:"id"`
}

type TypeDto struct {
	Type string `json:"type"`
}

type ListByParentIdDto struct {
	ParentId *uint64             `json:"parentId"`
	Opt      models2.ListOptions `json:"opt"`
}

type ListBySlugDto struct {
	Slug *string             `json:"slug"`
	Opt  models2.ListOptions `json:"opt"`
}

type ListResp struct {
	List interface{} `json:"list"`
}

type GetBySlugResp struct {
	Item          *models.Entity `json:"item"`
	ChildrenCount uint64         `json:"childrenCount"`
}

type ListWithParentResp struct {
	List      interface{}        `json:"list"`
	Paginator *models2.Paginator `json:"paginator"`
}

type CountResp struct {
	Count int64 `json:"count"`
}
