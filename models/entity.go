package models

import (
	"github.com/bxcodec/faker/v3"
	"gitlab.com/nikita.morozov/ms-shared/models"
)

type Entity struct {
	models.BaseUInt64Model
	Slug      *string            `json:"slug"`
	ChildType *string            `json:"childType"`
	Type      string             `json:"type"`
	Data      models.JSONB       `json:"data" gorm:"type:jsonb;not null;default:'{}'"`
	Title     string             `json:"title"`
	ParentId  *uint64            `json:"parentId"`
	Path      models.JSONBUint64 `json:"path" gorm:"type:jsonb;not null;default:'[]'"`
}

func (m Entity) FakeDataEntity() *Entity {
	var a Entity
	err := faker.FakeData(&a)
	if err == nil {
		return &a
	}

	a = Entity{}
	faker.FakeData(&a.ID)
	faker.FakeData(&a.CreatedAt)
	faker.FakeData(&a.UpdatedAt)
	faker.FakeData(&a.Slug)
	faker.FakeData(&a.ChildType)
	faker.FakeData(&a.Type)
	faker.FakeData(&a.Title)
	faker.FakeData(a.Data)
	faker.FakeData(&a.Path)
	faker.FakeData(&a.ParentId)

	return &a
}

func (u Entity) MakeParentPath() *[]uint64 {
	if u.Path != nil {
		var items = []uint64(u.Path)
		path := make([]uint64, len(items)+1)
		copy(path, items)
		// TODO: Maybe buggy
		path[len(items)] = u.ID

		return &path
	}

	return nil
}
