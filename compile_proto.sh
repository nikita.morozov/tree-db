#!/bin/bash

PROJECT=tree

protoc --proto_path=../$PROJECT-proto --proto_path=../ms-shared/proto  --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    ../$PROJECT-proto/entity.proto