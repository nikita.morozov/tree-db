package usecases

import (
	"gitlab.com/aquix/ms-generator/tree/models"
	"gitlab.com/aquix/ms-generator/tree/repositories"

	models2 "gitlab.com/nikita.morozov/ms-shared/models"
)

type EntityUseCases interface {
	Create(item *models.Entity) error
	Update(item *models.Entity) (*models.Entity, error)
	Delete(id uint64) error
	Count() (int64, error)
	CountById(id *uint64) (int64, error)
	GetById(id uint64) (*models.Entity, error)
	GetByType(typeValue string) (*models.Entity, error)
	GetBySlug(slug string) (*models.Entity, error)
	ListByParentId(parentId *uint64, opts models2.ListOptions) (*[]models.Entity, *models2.Paginator, error)
}

type entityUseCase struct {
	repo repositories.EntityRepo
}

func (t *entityUseCase) GetParentPath(parentId *uint64) (*[]uint64, error) {
	if parentId == nil {
		return nil, nil
	}

	item, err := t.GetById(*parentId)
	if err != nil {
		return nil, err
	}

	return item.MakeParentPath(), nil
}

func (t *entityUseCase) Create(item *models.Entity) error {
	if item.ParentId != nil {
		var path *[]uint64
		path, err := t.GetParentPath(item.ParentId)
		if err != nil {
			path = &[]uint64{}
		}

		item.Path = *path
	}

	err := t.repo.Create(item)

	return err
}

func (t *entityUseCase) Update(item *models.Entity) (*models.Entity, error) {
	res, err := t.repo.Update(item)
	if err != nil {
		return nil, err
	}

	return res, err
}

func (t *entityUseCase) Delete(id uint64) error {
	err := t.repo.Delete(id)

	return err
}

func (t *entityUseCase) GetById(id uint64) (*models.Entity, error) {
	return t.repo.GetById(id)
}

func (t *entityUseCase) GetBySlug(slug string) (*models.Entity, error) {
	return t.repo.GetBySlug(slug)
}

func (t *entityUseCase) Count() (int64, error) {
	return t.repo.Count()
}

func (t *entityUseCase) GetByType(typeValue string) (*models.Entity, error) {
	return t.repo.GetByType(typeValue)
}

func (t *entityUseCase) CountById(id *uint64) (int64, error) {
	return t.repo.CountChildren(id)
}

func (t *entityUseCase) ListByParentId(parentId *uint64, opts models2.ListOptions) (*[]models.Entity, *models2.Paginator, error) {
	count, err := t.repo.CountChildren(parentId)
	if err != nil {
		return nil, nil, err
	}

	list, err := t.repo.ListByParentId(parentId, opts)

	return list, &models2.Paginator{
		Offset:       opts.Offset,
		TotalCount:   int(count),
		CountPerPage: opts.Limit,
	}, err
}

func NewEntityUseCase(repo repositories.EntityRepo) EntityUseCases {
	return &entityUseCase{
		repo: repo,
	}
}
