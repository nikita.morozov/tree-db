package usecases

import (
	"errors"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aquix/ms-generator/tree/models"
	"gitlab.com/aquix/ms-generator/tree/repositories/mocks"
	models2 "gitlab.com/nikita.morozov/ms-shared/models"

	"testing"
)

type EntitySuite struct {
	suite.Suite

	uc         EntityUseCases
	repoEntity *mocks.EntityRepo
}

func (s *EntitySuite) BeforeTest(_, _ string) {
	repoEntity := new(mocks.EntityRepo)

	s.uc = NewEntityUseCase(repoEntity)
	s.repoEntity = repoEntity
}

func TestEntityUC(t *testing.T) {
	suite.Run(t, new(EntitySuite))
}

func (s *EntitySuite) TestEntity_ucCreate() {
	fakeItem := models.Entity{}.FakeDataEntity()

	item := models.Entity{
		Type:     fakeItem.Type,
		Data:     fakeItem.Data,
		Title:    fakeItem.Title,
		ParentId: fakeItem.ParentId,
		Path:     fakeItem.Path,
	}
	s.repoEntity.On("GetById", *item.ParentId).Return(nil, errors.New("not-found"))
	s.repoEntity.On("Create", &item).Return(nil)

	err := s.uc.Create(&item)
	require.NoError(s.T(), err)
}

func (s *EntitySuite) TestEntity_ucUpdate() {
	fakeItem := models.Entity{}.FakeDataEntity()

	item := models.Entity{
		BaseUInt64Model: models2.BaseUInt64Model{ID: fakeItem.ID},
		Type:            fakeItem.Type,
		Data:            fakeItem.Data,
		Title:           fakeItem.Title,
		ParentId:        fakeItem.ParentId,
		Path:            fakeItem.Path,
	}
	s.repoEntity.On("Update", &item).Return(&item, nil)

	res, err := s.uc.Update(&item)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
}

func (s *EntitySuite) TestEntity_ucDelete() {
	id := uint64(1)

	s.repoEntity.On("Delete", id).Return(nil)

	err := s.uc.Delete(id)
	require.NoError(s.T(), err)
}

func (s *EntitySuite) TestEntity_ucCount() {
	countRes := int64(1)

	s.repoEntity.On("Count").Return(countRes, nil)

	count, err := s.uc.Count()
	require.NoError(s.T(), err)
	require.Equal(s.T(), countRes, count)
}

func (s *EntitySuite) TestEntity_ucGetById() {
	fakeItem := models.Entity{}.FakeDataEntity()
	item := models.Entity{
		BaseUInt64Model: models2.BaseUInt64Model{ID: fakeItem.ID},
		Type:            fakeItem.Type,
		Data:            fakeItem.Data,
		Title:           fakeItem.Title,
		ParentId:        fakeItem.ParentId,
		Path:            fakeItem.Path,
	}

	s.repoEntity.On("GetById", item.ID).Return(&item, nil)

	res, err := s.uc.GetById(item.ID)
	require.NoError(s.T(), err)
	require.Equal(s.T(), *res, item)
}
func (s *EntitySuite) TestEntity_ucGetByType() {
	fakeItem := models.Entity{}.FakeDataEntity()
	item := models.Entity{
		BaseUInt64Model: models2.BaseUInt64Model{ID: fakeItem.ID},
		Type:            fakeItem.Type,
		Data:            fakeItem.Data,
		Title:           fakeItem.Title,
		ParentId:        fakeItem.ParentId,
		Path:            fakeItem.Path,
	}

	s.repoEntity.On("GetByType", item.Type).Return(&item, nil)

	res, err := s.uc.GetByType(item.Type)
	require.NoError(s.T(), err)
	require.Equal(s.T(), *res, item)
}
func (s *EntitySuite) TestEntity_ucListByParentId() {
	fakeItem := models.Entity{}.FakeDataEntity()
	fakeItemTwo := models.Entity{}.FakeDataEntity()
	fakeParentItem := models.Entity{}.FakeDataEntity()

	parentId1 := uint64(1)
	parentId2 := uint64(2)
	parentId3 := uint64(3)

	fakeParentItem.ID = 3
	fakeParentItem.ParentId = &parentId3

	items := []models.Entity{
		{
			BaseUInt64Model: models2.BaseUInt64Model{ID: 1},
			Type:            fakeItem.Type,
			Data:            fakeItem.Data,
			Title:           fakeItem.Title,
			ParentId:        &parentId1,
			Path:            fakeItem.Path,
		},
		{
			BaseUInt64Model: models2.BaseUInt64Model{ID: 2},
			Type:            fakeItemTwo.Type,
			Data:            fakeItemTwo.Data,
			Title:           fakeItemTwo.Title,
			ParentId:        &parentId2,
			Path:            fakeItemTwo.Path,
		},
	}

	opts := models2.ListOptions{
		Limit:  1,
		Offset: 0,
	}
	s.repoEntity.On("GetById", (*fakeParentItem).ID).Return(fakeParentItem, nil)
	s.repoEntity.On("CountChildren", &fakeParentItem.ID).Return(int64(10), nil)
	s.repoEntity.On("ListByParentId", &fakeParentItem.ID, opts).Return(&items, nil)

	result, _, err := s.uc.ListByParentId(&fakeParentItem.ID, opts)
	require.NoError(s.T(), err)
	require.Equal(s.T(), len(*result), len(items))
}
