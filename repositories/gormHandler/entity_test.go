package gormHandler

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aquix/ms-generator/tree/models"
	"gitlab.com/aquix/ms-generator/tree/repositories"
	models2 "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
)

type EntitySuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.EntityRepo
}

func (s *EntitySuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "tree-ms_",
		},
	})
	require.NoError(s.T(), err)
	s.repository = NewEntityRepository(s.DB, 10)
}

func (s *EntitySuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestEntity(t *testing.T) {
	suite.Run(t, new(EntitySuite))
}

func (s *EntitySuite) Test_gormEntityCreate() {
	fakeItem := models.Entity{}.FakeDataEntity()

	item := models.Entity{
		Type:     fakeItem.Type,
		Data:     fakeItem.Data,
		Title:    fakeItem.Title,
		ParentId: fakeItem.ParentId,
		Path:     fakeItem.Path,
	}

	s.mock.ExpectBegin()

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "tree-ms_entities" ("created_at","updated_at","deleted_at","slug","child_type","type","data","title","parent_id","path") VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) RETURNING "id"`)).
		WithArgs(models2.Any{}, models2.Any{}, nil, item.Slug, item.ChildType, item.Type, "{}", item.Title, item.ParentId, item.Path).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).
			AddRow(1))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)

	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint64(1))
}

func (s *EntitySuite) Test_gormEntityUpdate() {
	fakeItem := models.Entity{}.FakeDataEntity()

	item := models.Entity{
		BaseUInt64Model: models2.BaseUInt64Model{ID: fakeItem.ID},
		Type:            fakeItem.Type,
		Data:            fakeItem.Data,
		Title:           fakeItem.Title,
		ParentId:        fakeItem.ParentId,
		Path:            fakeItem.Path,
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "tree-ms_entities" SET "created_at"=$1,"updated_at"=$2,"deleted_at"=$3,"slug"=$4,"child_type"=$5,"type"=$6,"data"=$7,"title"=$8,"parent_id"=$9,"path"=$10 WHERE "id" = $11 AND "tree-ms_entities"."deleted_at" IS NULL`)).
		WithArgs(models2.Any{}, models2.Any{}, nil, item.Slug, item.ChildType, item.Type, item.Data, item.Title, item.ParentId, item.Path, item.ID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	res, err := s.repository.Update(&item)

	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
	require.Equal(s.T(), fakeItem.ID, res.ID)
}

func (s *EntitySuite) Test_gormEntityDelete() {
	fakeItem := models.Entity{}.FakeDataEntity()
	item := models.Entity{
		BaseUInt64Model: models2.BaseUInt64Model{ID: fakeItem.ID},
		Type:            fakeItem.Type,
		Data:            fakeItem.Data,
		Title:           fakeItem.Title,
		ParentId:        fakeItem.ParentId,
		Path:            fakeItem.Path,
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "tree-ms_entities" SET "deleted_at"=$1 WHERE (path @> $2 or id = $3) AND "tree-ms_entities"."deleted_at" IS NULL`)).
		WithArgs(models2.Any{}, item.ID, item.ID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(item.ID)

	require.NoError(s.T(), err)
}

func (s *EntitySuite) Test_gormEntityGetById() {
	fakeItem := models.Entity{}.FakeDataEntity()
	item := models.Entity{
		BaseUInt64Model: models2.BaseUInt64Model{ID: fakeItem.ID},
		Type:            fakeItem.Type,
		Data:            fakeItem.Data,
		Title:           fakeItem.Title,
		ParentId:        fakeItem.ParentId,
		Path:            fakeItem.Path,
	}

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "tree-ms_entities" WHERE "tree-ms_entities"."id" = $1 AND "tree-ms_entities"."deleted_at" IS NULL ORDER BY "tree-ms_entities"."id" LIMIT 1`)).
		WithArgs(item.ID).
		WillReturnRows(sqlmock.NewRows([]string{"id", "type", "data", "title", "parent_id", "path"}).
			AddRow(item.ID, item.Type, item.Data, item.Title, item.ParentId, item.Path))

	result, err := s.repository.GetById(item.ID)

	require.NoError(s.T(), err)
	require.Equal(s.T(), result.Type, item.Type)
	require.Equal(s.T(), result.Data, item.Data)
	require.Equal(s.T(), result.Title, item.Title)
	require.Equal(s.T(), result.ParentId, item.ParentId)
	require.Equal(s.T(), result.Path, item.Path)
}

func (s *EntitySuite) Test_gormEntityCount() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT count(*) FROM "tree-ms_entities" WHERE "tree-ms_entities"."deleted_at" IS NULL`)).
		WithArgs().
		WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(2))

	res, err := s.repository.Count()

	require.NoError(s.T(), err)
	require.Equal(s.T(), int(res), int(2))
}
func (s *EntitySuite) Test_gormEntityGetByType() {
	fakeItem := models.Entity{}.FakeDataEntity()
	item := models.Entity{
		BaseUInt64Model: models2.BaseUInt64Model{ID: fakeItem.ID},
		Type:            fakeItem.Type,
		Data:            fakeItem.Data,
		Title:           fakeItem.Title,
		ParentId:        fakeItem.ParentId,
		Path:            fakeItem.Path,
	}

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "tree-ms_entities" WHERE type = $1 AND "tree-ms_entities"."deleted_at" IS NULL ORDER BY "tree-ms_entities"."id" LIMIT 1`)).
		WithArgs(item.Type).
		WillReturnRows(sqlmock.NewRows([]string{"id", "type", "data", "title", "parent_id", "path"}).
			AddRow(item.ID, item.Type, item.Data, item.Title, item.ParentId, item.Path))

	result, err := s.repository.GetByType(item.Type)

	require.NoError(s.T(), err)
	require.Equal(s.T(), result.Type, item.Type)
	require.Equal(s.T(), result.Data, item.Data)
	require.Equal(s.T(), result.Title, item.Title)
	require.Equal(s.T(), result.ParentId, item.ParentId)
	require.Equal(s.T(), result.Path, item.Path)
}
func (s *EntitySuite) Test_gormEntityGetByParentId() {
	fakeItem := models.Entity{}.FakeDataEntity()
	fakeItemTwo := models.Entity{}.FakeDataEntity()
	items := []models.Entity{
		{
			BaseUInt64Model: models2.BaseUInt64Model{ID: fakeItem.ID},
			Type:            fakeItem.Type,
			Data:            fakeItem.Data,
			Title:           fakeItem.Title,
			ParentId:        fakeItem.ParentId,
			Path:            fakeItem.Path,
		},
		{
			BaseUInt64Model: models2.BaseUInt64Model{ID: fakeItemTwo.ID},
			Type:            fakeItemTwo.Type,
			Data:            fakeItemTwo.Data,
			Title:           fakeItemTwo.Title,
			ParentId:        fakeItemTwo.ParentId,
			Path:            fakeItemTwo.Path,
		},
	}

	opts := models2.ListOptions{
		Limit:  2,
		Offset: 0,
	}

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "tree-ms_entities" WHERE parent_id = $1 AND "tree-ms_entities"."deleted_at" IS NULL ORDER BY id desc LIMIT 2`)).
		WithArgs(fakeItem.ParentId).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).
			AddRow(1).
			AddRow(2))

	list, err := s.repository.ListByParentId(fakeItem.ParentId, opts)

	require.NoError(s.T(), err)
	require.Equal(s.T(), len(*list), len(items))
}
