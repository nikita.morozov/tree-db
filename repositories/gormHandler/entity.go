package gormHandler

import (
	"errors"
	"gitlab.com/aquix/ms-generator/tree/models"
	"gitlab.com/aquix/ms-generator/tree/repositories"
	models2 "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
)

type gormEntityRepository struct {
	DB    *gorm.DB
	limit int
}

func (r *gormEntityRepository) Create(item *models.Entity) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormEntityRepository) Update(item *models.Entity) (*models.Entity, error) {
	result := r.DB.Save(&item)
	return item, result.Error
}

func (r *gormEntityRepository) Delete(id uint64) error {
	result := r.DB.Where("path @> ? or id = ?", id, id).Delete(&models.Entity{})
	return result.Error
}

func (r *gormEntityRepository) Count() (int64, error) {
	var count int64
	result := r.DB.Model(&models.Entity{}).Count(&count)
	return count, result.Error
}

func (r *gormEntityRepository) CountChildren(parentId *uint64) (int64, error) {
	var count int64

	if parentId != nil {
		result := r.DB.Model(&models.Entity{}).Where("parent_id = ?", parentId).Count(&count)
		return count, result.Error
	}

	result := r.DB.Model(&models.Entity{}).Where("parent_id is null").Count(&count)
	return count, result.Error
}

func (r *gormEntityRepository) GetById(id uint64) (*models.Entity, error) {
	var item *models.Entity
	result := r.DB.First(&item, id)

	if item.ID == 0 {
		return nil, errors.New("repository.item-not-found")
	}

	return item, result.Error
}

func (r *gormEntityRepository) GetByType(typeValue string) (*models.Entity, error) {
	var item *models.Entity
	result := r.DB.Where("type = ?", typeValue).First(&item)
	return item, result.Error
}

func (r *gormEntityRepository) GetBySlug(slug string) (*models.Entity, error) {
	var item *models.Entity
	result := r.DB.Where("slug = ?", slug).First(&item)
	return item, result.Error
}

func (r *gormEntityRepository) ListByParentId(parentId *uint64, opts models2.ListOptions) (*[]models.Entity, error) {
	var items *[]models.Entity

	if parentId == nil {
		result := r.DB.Where("parent_id is null").Limit(opts.GetLimit(r.limit)).Offset(opts.Offset).Order(opts.GetOrder()).Find(&items)
		return items, result.Error
	}

	result := r.DB.Where("parent_id = ?", parentId).Limit(opts.GetLimit(r.limit)).Offset(opts.Offset).Order(opts.GetOrder()).Find(&items)
	return items, result.Error
}

func NewEntityRepository(DB *gorm.DB, limit int) repositories.EntityRepo {
	return &gormEntityRepository{
		DB:    DB,
		limit: limit,
	}
}
