package repositories

import (
	"gitlab.com/aquix/ms-generator/tree/models"
	models2 "gitlab.com/nikita.morozov/ms-shared/models"
)

type EntityRepo interface {
	Create(item *models.Entity) error
	Update(item *models.Entity) (*models.Entity, error)
	GetById(id uint64) (*models.Entity, error)
	Count() (int64, error)
	CountChildren(parentId *uint64) (int64, error)
	Delete(id uint64) error
	GetBySlug(slug string) (*models.Entity, error)
	GetByType(typeValue string) (*models.Entity, error)
	ListByParentId(parentId *uint64, opts models2.ListOptions) (*[]models.Entity, error)
}
