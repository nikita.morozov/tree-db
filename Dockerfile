FROM golang:1.17.6-alpine3.15 AS test

RUN apk update && apk add --no-cache git

ENV USER=appuser
ENV UID=10001
ENV CGO_ENABLED=0

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

RUN apk update && apk upgrade && apk add --no-cache ca-certificates
RUN update-ca-certificates

WORKDIR $GOPATH/src/gitlab.com/aquix/tree-ms/
COPY . .

# Using go mod.
RUN go mod tidy
# Build the binary.
RUN /bin/sh test.sh

############################
# STEP 1 build executable binary
############################
FROM golang:1.17.6-alpine3.15 AS builder

RUN apk update && apk add --no-cache git

ARG TARGETOS
ARG TARGETARCH
ARG VERSION=v0.0.0-alpha
ENV USER=appuser
ENV UID=10001
ENV CGO_ENABLED=0

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

RUN apk update && apk upgrade && apk add --no-cache ca-certificates
RUN update-ca-certificates

WORKDIR $GOPATH/src/gitlab.com/aquix/tree-ms/
COPY . .

RUN GRPC_HEALTH_PROBE_VERSION=v0.4.5 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-${TARGETARCH} && \
    chmod +x /bin/grpc_health_probe

# Using go mod.
RUN go mod tidy
# Build the binary.
RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -ldflags="-w -s" -o /go/bin/tree
############################
# STEP 2 build a small image
############################
FROM scratch

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

COPY --from=builder /go/bin/tree /go/bin/tree
COPY --from=builder /bin/grpc_health_probe /bin/grpc_health_probe
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

USER appuser:appuser

ENTRYPOINT ["/go/bin/tree"]
CMD ["${VERSION}"]
