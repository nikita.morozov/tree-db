#!/bin/bash

ROOT_PATH="/Users/evgeniyzavgorodniy/go/src/gitlab.com/aquix/ms-generator/tree"
echo "$ROOT_PATH"


cd $ROOT_PATH/repositories
bash --rcfile .mybashrc -ci 'mockery --all'

cd $ROOT_PATH/usecases
bash --rcfile .mybashrc -ci 'mockery --all'
